<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){ 
        return view('register');
    }
    public function welcome(Request $request){
         /* dd($request->all()); */
         $namaAwal = $request['firstname'];
         $namaAkhir = $request['lastname'];
         return view('welcome', compact('namaAwal','namaAkhir'));
    }
         
    
}
