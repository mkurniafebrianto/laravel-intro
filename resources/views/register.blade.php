<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome">
        <label>First name:</label> <br><br>
        <input type="text" name="firstname"> 
        <br><br>
        <label>Last name:</label> <br><br>
        <input type="text" name="lastname"> 
        <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="WN">Male <br>
        <input type="radio" name="WN">Female <br>
        <input type="radio" name="WN">Others 
        <br><br>
        <label for="">Nationality:</label><br><br>
        <select name="Nationality">
            <option value="1">Indonesian</option>
        </select>
        <br><br>
        <label for="">Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Others
        <br><br>
        <label for="">Bio:</label> <br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>